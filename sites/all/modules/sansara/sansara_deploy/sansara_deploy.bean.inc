<?php
/**
 * @file
 * sansara_deploy.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function sansara_deploy_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'static_block';
  $bean_type->label = 'static block';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['static_block'] = $bean_type;

  return $export;
}
