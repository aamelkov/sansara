<?php
/**
 * @file
 * sansara_deploy.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function sansara_deploy_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: management.
  $menus['management'] = array(
    'menu_name' => 'management',
    'title' => 'Management',
    'description' => 'The <em>Management</em> menu contains links for administrative tasks.',
  );
  // Exported menu: menu-center-menu.
  $menus['menu-center-menu'] = array(
    'menu_name' => 'menu-center-menu',
    'title' => 'Center menu',
    'description' => '',
  );
  // Exported menu: menu-left-menu.
  $menus['menu-left-menu'] = array(
    'menu_name' => 'menu-left-menu',
    'title' => 'Left menu',
    'description' => '',
  );
  // Exported menu: menu-right-menu.
  $menus['menu-right-menu'] = array(
    'menu_name' => 'menu-right-menu',
    'title' => 'Right menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Center menu');
  t('Left menu');
  t('Main menu');
  t('Management');
  t('Right menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');
  t('The <em>Management</em> menu contains links for administrative tasks.');

  return $menus;
}
