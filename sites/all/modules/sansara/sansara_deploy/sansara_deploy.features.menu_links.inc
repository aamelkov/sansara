<?php
/**
 * @file
 * sansara_deploy.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function sansara_deploy_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_add-a-product:admin/commerce/products/add.
  $menu_links['management_add-a-product:admin/commerce/products/add'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/commerce/products/add',
    'router_path' => 'admin/commerce/products/add',
    'link_title' => 'Add a product',
    'options' => array(
      'attributes' => array(
        'title' => 'Add a new product for sale.',
      ),
      'identifier' => 'management_add-a-product:admin/commerce/products/add',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 10,
    'customized' => 0,
    'parent_identifier' => 'management_products:admin/commerce/products',
  );
  // Exported menu link: management_create-name:admin/commerce/products/add/product.
  $menu_links['management_create-name:admin/commerce/products/add/product'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/commerce/products/add/product',
    'router_path' => 'admin/commerce/products/add/product',
    'link_title' => 'Create !name',
    'options' => array(
      'attributes' => array(
        'title' => 'A basic product type.',
      ),
      'identifier' => 'management_create-name:admin/commerce/products/add/product',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_add-a-product:admin/commerce/products/add',
  );
  // Exported menu link: management_generate-products:admin/config/development/generate/product.
  $menu_links['management_generate-products:admin/config/development/generate/product'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/development/generate/product',
    'router_path' => 'admin/config/development/generate/product',
    'link_title' => 'Generate products',
    'options' => array(
      'attributes' => array(
        'title' => 'Generate a given number of products. Optionally delete current products.',
      ),
      'identifier' => 'management_generate-products:admin/config/development/generate/product',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_development:admin/config/development',
  );
  // Exported menu link: management_product-pricing-rules:admin/commerce/config/product-pricing.
  $menu_links['management_product-pricing-rules:admin/commerce/config/product-pricing'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/commerce/config/product-pricing',
    'router_path' => 'admin/commerce/config/product-pricing',
    'link_title' => 'Product pricing rules',
    'options' => array(
      'attributes' => array(
        'title' => 'Enable and configure product pricing rules and pre-calculation.',
      ),
      'identifier' => 'management_product-pricing-rules:admin/commerce/config/product-pricing',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_configuration:admin/commerce/config',
  );
  // Exported menu link: management_product:admin/commerce/config/line-items/product.
  $menu_links['management_product:admin/commerce/config/line-items/product'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/commerce/config/line-items/product',
    'router_path' => 'admin/commerce/config/line-items/product',
    'link_title' => 'Product',
    'options' => array(
      'identifier' => 'management_product:admin/commerce/config/line-items/product',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_line-item-types:admin/commerce/config/line-items',
  );
  // Exported menu link: management_products:admin/commerce/products.
  $menu_links['management_products:admin/commerce/products'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/commerce/products',
    'router_path' => 'admin/commerce/products',
    'link_title' => 'Products',
    'options' => array(
      'attributes' => array(
        'title' => 'Manage products and product types in the store.',
      ),
      'identifier' => 'management_products:admin/commerce/products',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'management_store:admin/commerce',
  );
  // Exported menu link: navigation_product:node/add/product.
  $menu_links['navigation_product:node/add/product'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/product',
    'router_path' => 'node/add/product',
    'link_title' => 'Product',
    'options' => array(
      'identifier' => 'navigation_product:node/add/product',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'navigation_add-content:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Add a product');
  t('Create !name');
  t('Generate products');
  t('Product');
  t('Product pricing rules');
  t('Products');

  return $menu_links;
}
