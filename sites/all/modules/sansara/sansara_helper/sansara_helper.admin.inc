<?php
/**
 * @file
 * Sansara settings form.
 */

/**
 * Settings form.
 */
function sansara_helper_settings() {
  $form = array();
  $form['sansara_soc_links'] = array(
    '#title' => t('Social links'),
    '#type' => 'textarea',
    '#default_value' => variable_get('sansara_soc_links', ''),
  );
  $form['sansara_splash'] = array(
    '#title' => t('Splash'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('sansara_splash', 0),
  );
  $form['sansara_token'] = array(
    '#title' => t('Token'),
    '#type' => 'textfield',
    '#default_value' => variable_get('sansara_token', '18317b7ba4'),
  );
  $form['sansara_web_form'] = array(
    '#title' => t('Web form ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('sansara_web_form', 167),
  );
  $form['sansara_policy'] = array(
    '#title' => t('Confidentiality Policy ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('sansara_policy', 167),
  );
  $form['sansara_sizes_guide'] = array(
    '#title' => t('Sizes guide Page ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('sansara_sizes_guide', 3),
  );
  return system_settings_form($form);
}
