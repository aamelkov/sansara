(function ($) {

  // Collection page, pager.
  $(document).on('click', '.pages li a', function () {
    $('.views-widget-per-page select').val($(this).text());
    $('.views-submit-button button').click();
    return false;
  });

  // Commerce customer profile copy, click by hidden checkbox.
  $(document).on('click', '.trigger-copy label', function () {
    if (!$(this).find('input:checked').length) {
      $('.commerce-customer-profile-copy label').trigger('click');
    }
  });

  // Basket hover.
  $(document).on('mouseenter', '.basket-icon', function () {
    $('.cart-popup').fadeIn();
  });

  $(document).on('mouseleave', '.cart-popup', function () {
    $('.cart-popup').fadeOut();
  });

  // Commerce customer profile copy, click by hidden checkbox.
  $(document).on('focusout', '.views-field-edit-quantity input', function () {
    $('.commerce-line-item-actions .form-submit').trigger('mousedown');
  });

  // Collection page, select.
  $(document).on('change', '.sub-header .form-select', function () {
    var a = $(this).val().toLowerCase();
    $('.views-widget-sort-sort_bef_combine select option').each(function () {
      var b = $(this).text().toLowerCase();
      if (a == b) {
        $(this).prop('selected', true);
      }
    });
    $('.views-submit-button button').click();
  });

  $(document).ready(function () {
    // Collection page, set select option by default.
    var page = $('.views-widget-per-page select').val();
    $('.sub-header li a:contains("' + page + '")').addClass('active');
    var a = $('.views-widget-sort-sort_bef_combine select option:selected').text().toLowerCase();
    $('.sub-header .form-select option').each(function () {
      var b = $(this).val().toLowerCase();
      if (a == b) {
        $(this).prop('selected', true);
      }
    });

    $('.search-trigger').click(function () {
      $('#block-search-form .form-search > div').slideToggle();
    });

    var splash = $('.splash-wrap').length;
    if (splash) {
      $('.splash-wrap').fadeOut(3000);
    }

    $(window).keypress(function (e) {
      // Enter space or enter.
      if (splash && (e.keyCode === 13 || e.keyCode === 32)) {
        $('.splash-wrap').remove();
        e.preventDefault();
      }
    });

    // Product page.
    $('.optionset-product ul.slides').addClass('col-md-10');
    $('.optionset-product .flex-control-thumbs').addClass('col-md-2');

    // Description toggle on product page.
    $('.node-product .field-name-body ul').each(function () {
      $(this).wrap('<div class="wrap-ul"></div>');
      var li = $.trim($(this).find('li').eq(0).text());
      $(this).before('<div class="ul-title">' + li + '</div>');
    });
    $('.ul-title').click(function () {
      $(this).next().slideToggle();
    });

    // Carousel.
    if ($('.view-collections .view-content').length) {
      $('.view-collections .view-content').Cloud9Carousel({
        buttonLeft: $('#buttons > .btn-left'),
        buttonRight: $('#buttons > .btn-right'),
        autoPlay: 1,
        bringToFront: true
      });
    }

    $('.view-filters .views-exposed-widget > label').click(function () {
      $(this).toggleClass('open').next().slideToggle();
    });

    $('.views-widget-filter-field_size_tid label').addClass('open');

  });


  Drupal.behaviors.sansara = {
    attach: function (context, settings) {
      $('label input').once('san', function () {
        if ($(this).next('img').length == 0) {
          $(this).after('<span></span>');
        }
        $('.form-item-attributes-field-size label span, label[for="edit-field-color-tid-all"] span').remove();
      });
      $('.form-item-attributes-field-size label input:checked').parent().addClass('active');
    }
  }
})(jQuery);

// Slider for references on product page.
(function ($) {
  $(window).load(function () {
    $('#block-views-flex-slider-block, #block-bean-static-block-front').show(0);

    if ($('.item-list .slides').length) {
      // Fixed for variable width of the items.
      total_width = 0;
      $('.item-list .slides > li img').each(function () {
        total_width += $(this).width();
      });
      avg_width = total_width / ($('.item-list .slides > li').length);
      $('.item-list').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: avg_width,
        itemMargin: 60,
        controlNav: false,
      });
    }
  });
})(jQuery);