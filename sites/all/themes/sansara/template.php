<?php
/**
 * @file
 * The primary PHP file for this theme.
 */

/**
 * Implementation hook_js_alter().
 */
function sansara_js_alter(&$javascript) {
  // Needs for theming fields password in page register.
  unset($javascript['sites/all/themes/bootstrap/js/modules/user/user.js']);
}

/**
 * Get specifix type theme.
 *
 * Return black or white.
 *
 * !refactoring.
 */
function sansara_type_theme() {

  $class = array();
  if (
    drupal_is_front_page() ||
    in_array(arg(0), array(
      'user',
      'checkout',
    ))
    && arg(2) !== 'complete'
    && arg(2) !== 'edit'
  ) {
    $class[] = 'black-theme';
  }
  else {
    $class[] = 'light-theme';
  }

  // User page for logged in.
  if (arg(0) == 'user' && count(arg()) == 2 && user_is_logged_in()) {
    $class = array('light-theme', 'user-account');
  }

  if (arg(0) == 'user' && !user_is_logged_in()) {
    $class = array('black-theme');
  }

  if (arg(0) == 'orders' && user_is_logged_in()) {
    $class = array('light-theme');
  }
  return $class;
}

/**
 * Implementation template_preprocess_user_profile().
 */
function sansara_preprocess_user_profile(&$vars) {
  global $user;

  $pid = sansara_get_pid();

  $info = t('Your personal information');
  $info = $pid ? l($info, 'admin/commerce/customer-profiles/' . $pid . '/edit') : l($info, '/admin/commerce/customer-profiles/add/billing');

  $orders = t('Your order\'s status or history');
  $orders = l($orders, '/orders');

  $returns = t('Information about your returns');
  $fitting = t('Your fitting room settings');
  $wishlist = t('Your favorite items');
  $wishlist = l($wishlist, '/wishlist');

  $subscriptions = t('Subscriptions settings');
  $subscriptions = l($subscriptions, '/user/' . $user->uid . '/edit/simplenews');

  $profile = commerce_single_address_active_profile_load($user->uid, 'billing');
  if ($profile) {
    $pid = $profile->profile_id;
    $content = entity_view('commerce_customer_profile', array($profile->profile_id => $profile), 'customer');
    $info .= drupal_render($content);
  }

  $html = '<div class="account-title">';
  $html .= t('Hello @user, this is your <i>Massimo Fedi</i> account', array(
    '@user' => $user->name,
  ));
  $html .= '</div>';
  $html .= '<div class="row">';
  $html .= '<div class="col-md-6">';

  foreach (
    array(
      'info' => t('My info'),
      'orders' => t('My Orders'),
      'returns' => t('Returns'),
//      'fitting' => t('Fitting room'),
      'wishlist' => t('Wishlist'),
      'subscriptions' => t('Subscriptions'),
    ) as $key => $i) {
    $html .= '<div class="info-item">';
    $html .= '<div class="info-label">' . $i . '</div>';
    $html .= '<div class="info-body">' . $$key . '</div>';
    $html .= '</div>';
  }
  $html .= '</div>';

  $html .= '<div class="col-md-6">';
  $html .= '<img src="/sites/all/themes/sansara/images/account.jpg">';
  $html .= '</div>';

  $html .= '</div>';

  $html .= '<div class="go-back">';
  $html .= l(t('Go to shop'), '');
  $html .= '</div>';
  $vars['user_profile']['#markup'] = $html;
}

/**
 * Implementation template_preprocess_html().
 */
function sansara_preprocess_html(&$vars) {
  $vars['classes_array'] = array_merge($vars['classes_array'], sansara_type_theme());

  $splash = '<div class="splash-wrap">';
  $splash .= '<img class="img-responsive" src="/sites/all/themes/sansara/images/splash.jpg">';
  $splash .= '</div>';
  // Check for show splash once.
  if (function_exists('sansara_lists_session') && sansara_lists_session('check_first')) {
    $splash = '';
  }
  $vars['page']['page_top']['#markup'] = variable_get('sansara_splash', 0) ? $splash : '';
  sansara_lists_session('check_first', 1);
}

/*
 * Implementation template_preprocess_page().
 */
function sansara_preprocess_page(&$vars) {
  global $user;
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), '/');
  $vars['site_name'] = NULL;
  $vars['page']['content']['commerce_multicurrency_currency_menu']['#access'] = TRUE;

  // Сhanged the logo file name for black theme.
  $logo = isset($vars['logo']) ? $vars['logo'] : FALSE;
  if ($logo
    && array_search('black-theme', sansara_type_theme()) !== FALSE
    && !drupal_is_front_page()
  ) {
    $vars['logo'] = str_replace('.png', '_black.png', $logo);
  }

  if (drupal_is_front_page()) {
    unset($vars['page']['content']['system_main']);
    unset($vars['messages']);
  }

  // Login link in header.
  $options = array();
  $options['html'] = TRUE;
  $options['attributes']['class'][] = 'login-url';
  if (user_is_anonymous()) {
    $options['attributes']['class'][] = 'not-logged';
    $vars['login_link'] = l(t('sing in'), '/user', $options);
  }
  else {
    $options['attributes']['class'][] = 'user-url';
    $icon = '<span title="Profile" class="icon-user" aria-hidden="true"></span>';
    $vars['login_link'] = l($icon, '/user', $options);
    $vars['login_link'] .= l(t('sign out'), '/user/logout', $options);
  }
  $countW = count(views_get_view_result('wishlist_count', $display_id = 'default'));
  $icon = '<span class="icon-wishlist"><span class="basket-count">' . $countW . '</span></span>';
  $options['attributes']['class'] = array('wishlist-url');
  $vars['wishlist'] = l($icon, '/wishlist', $options);

  // Login, register page.
  if (in_array(arg(0), array(
      'user',
    ))
    && user_is_anonymous()
  ) {
    drupal_set_title('');
    drupal_set_breadcrumb(array());
    unset($vars['tabs']['#primary'][2]);
    $vars['tabs']['#primary'][1]['#link']['title'] = t('Login');
    $vars['tabs']['#primary'][0]['#link']['title'] = t('Register');
    krsort($vars['tabs']['#primary']);
  }

  // Product page.
  if (isset($vars['node']) && $vars['node']->type == 'product') {
    drupal_set_title('');
    $breadcrumb[] = l(t('Collections'), 'collections');
    $breadcrumb[] = l(t($vars['node']->field_collections['und'][0]['taxonomy_term']->name), drupal_get_path_alias('/taxonomy/term/' . $vars['node']->field_collections['und'][0]['tid']));
    $breadcrumb[] = l(t($vars['node']->field_categories['und'][0]['taxonomy_term']->name), drupal_get_path_alias('/taxonomy/term/' . $vars['node']->field_categories['und'][0]['tid']));
    $breadcrumb[] = t($vars['node']->title);
    drupal_set_breadcrumb($breadcrumb);
  }

  // Collections page.
  if (current_path() == 'collections') {
    drupal_set_title('');
    $options['attributes']['class'][] = 'show-more';
    $more = '<div class="more">' . l(t('Show more'), '', $options) . '</div>';
    $vars['page']['content']['system_main']['main']['#markup'] .= $more;
  }

  // Checkout page.
  if (current_path() == 'cart') {
    $count = sansara_get_count_items();
    drupal_set_title(t('YOUR SHOPPING CART') . $count, PASS_THROUGH);
    $cart = $vars['page']['content']['system_main']['main']['#markup'];
    $options['attributes']['class'][] = 'continue-cart';
    $html = l('Continue Shopping', '/catalog', $options);
    $vars['page']['content']['system_main']['main']['#markup'] = $html . $cart;
    drupal_set_breadcrumb(array());
  }

  $html = '<div class="row steps">';
  $html .= '<div class="step-1 col-md-4">' . t('DELIVERY') . '</div>';
  $html .= '<div class="step-2 col-md-4">' . t('PAYMENT') . '</div>';
  $html .= '<div class="step-3 col-md-4">' . t('DONE') . '</div>';
  $html .= '</div>';

  // Checkout page, step 1.
  $vars['steps'] = NULL;
  if (arg(0) == 'checkout') {
    drupal_set_title('');
    drupal_set_breadcrumb(array());
    $vars['steps'] = str_replace('step-1', 'step-1 active ', $html);
  }

  // Checkout page, step 2.
  if (arg(0) == 'checkout' && arg(2) == 'review') {
    $html = str_replace('step-1', 'step-1 done', $html);
    $html = str_replace('step-2', 'step-2 active ', $html);
    $html = str_replace('step-3', 'step-3', $html);
    $vars['steps'] = $html;
  }

  // Checkout page, step 3.
  if (arg(0) == 'checkout' && arg(2) == 'complete') {
    $html = str_replace('step-1', 'step-1 done ', $html);
    $html = str_replace('step-2', 'step-2 done ', $html);
    $html = str_replace('step-3', 'step-3 active ', $html);
    $vars['steps'] = $html;
    $slider = views_embed_view('flex_random', $display_id = 'default');
    $vars['page']['content']['flex']['#markup'] = $slider;
    drupal_add_library('flexslider', 'flexslider');
  }

  // Carousel page.
  if (current_path() == 'carousel') {
    drupal_set_title('');
    drupal_set_breadcrumb(array());
  }

  // Catalog page.
  $vars['list_page'] = NULL;
  if (arg(0) == 'taxonomy' && arg(1) == 'term' || arg(0) == 'catalog') {
    $items = array();
    foreach (array(10, 18, 20, 40, 60) as $i) {
      $items[] = l($i, NULL);
    }

    $vars['list_page'] = '<div class="list-page">' . t('See');
    $vars['list_page'] .= theme_item_list(array(
      'items' => $items,
      'type' => 'ul',
      'title' => NULL,
      'attributes' => array('class' => array('pages'))
    ));
    $vars['list_page'] .= '</div>';

    $options = array(
      'Price Asc',
      'Price Desc',
      'Title Asc',
      'Title Desc',
    );

    $vars['list_page'] .= '<div class="list-sort">' . t('Sort') . ': ';
    $vars['list_page'] .= theme('select',
      array('element' => array('#options' => drupal_map_assoc($options)))
    );
    $vars['list_page'] .= '</div>';

    if ((int) arg(2) > 0) {
      $term = taxonomy_term_load((int) arg(2));
      drupal_set_title('');
      if ($term->vid == 3) {
        $breadcrumb[] = l(t('Collections'), '/collections');
      }
      if ($term->vid == 2) {
        $breadcrumb[] = l(t('Catalog'), '/catalog');
      }
      $breadcrumb[] = $term->name;
      drupal_set_breadcrumb($breadcrumb);
    }

    if (arg(0) == 'catalog') {
      $breadcrumb[] = l(t('Catalog'), '/catalog');
      drupal_set_breadcrumb($breadcrumb);
    }
    unset($vars['tabs']);
  }

  if ((arg(0) == 'collections' || arg(0) == 'wishlist' || arg(0) == 'search' || arg(0) == 'orders' || arg(0) == 'faq') && count(arg()) == 1) {
    drupal_set_breadcrumb(array());
  }

  // Account.
  if (arg(0) == 'user' && arg(2) == 'edit') {
    drupal_set_title('');
    $vars['tabs']['#access'] = FALSE;
    if (arg(3) == 'simplenews') {
      drupal_set_title(t('Subscriptions'));
    }
  }
  if (arg(0) == 'user' && count(arg()) == 2) {
    drupal_set_title(t('My account'));
    $vars['tabs']['#access'] = FALSE;
    drupal_set_breadcrumb(array());
  }

  if (arg(0) == 'user' && arg(1) == 'register') {
    $vars['tabs']['#access'] = TRUE;
    drupal_set_title('');
    drupal_set_breadcrumb(array());
  }

  if (isset($vars['node']) && $vars['node']->type !== 'product') {
    $breadcrumb[] = $vars['node']->title;
    drupal_set_breadcrumb($breadcrumb);
    $vars['page']['content']['commerce_multicurrency_currency_menu']['#access'] = TRUE;
  }

  if (arg(0) == 'admin') {
    unset($vars['page']['content']['commerce_multicurrency_currency_menu']);
    drupal_set_breadcrumb(array());
  }
}

/**
 * Implementation template_preprocess_node().
 */
function sansara_preprocess_node(&$vars) {
  $vars['content']['language']['#access'] = FALSE;
  if ($vars['type'] == 'article' && isset($vars['field_image'][0]['fid'])) {
    $vars['classes_array'][] = 'row';
  }
  // Slider for references on product page.
  $vars['references_slider'] = NULL;
  if (
    $vars['type'] == 'product' &&
    isset($vars['field_references']) &&
    count($vars['field_references']) > 0
  ) {
    $items = array();
    foreach ($vars['field_references'] as $key => $item) {
      if (!isset($item['node'])) {
        continue;
      }
      $img = theme('image_style', array(
        'style_name' => 'thumb-280x380',
        'path' => $item['node']->field_images['und'][0]['uri'],
        'attributes' => array('class' => array('references-slider')),
      ));
      $items[] = l($img, 'node/' . $item['node']->nid, array('html' => TRUE));
    }
    if (count($items) > 0) {
      $vars['references_slider'] = theme_item_list(array(
        'items' => $items,
        'title' => t('Suggestions'),
        'type' => 'ul',
        'attributes' => array('class' => array('slides'))
      ));
    }
  }
}

/**
 * Implementation template_preprocess_field().
 */
function sansara_preprocess_field(&$vars) {
  // Arrow for static block.
  if (isset($vars['items'][0]['#element']['title'])) {
    $vars['items'][0]['#element']['title'] .= '<span></span>';
  }

  // Fields images, body on page article.
  if ($vars['element']['#bundle'] == 'article') {
    switch ($vars['element']['#field_name']) {
      case 'field_image' :
        $vars['classes_array'][] = 'col-md-3';
        break;
      case 'body' :
        $vars['classes_array'][] = 'col-md-9';
        break;
    }
  }
}

/**
 * Implementation template_preprocess_region().
 */
function sansara_preprocess_region(&$vars) {
  if (isset($vars['region'])) {
    if ($vars['region'] == "footer") {
      $vars['classes_array'][] = 'row';
      $vars['classes_array'][] = 'col-md-6';
    }
    if ($vars['region'] == "soc_links") {
      $vars['classes_array'][] = 'col-md-6';
    }
  }
}

/**
 * Implementation template_preprocess_block().
 */
function sansara_preprocess_block(&$vars) {
  if (in_array($vars['block']->region, array(
      'footer',
      'soc_links',
      'lang_switcher',
      'content',
      'header_right',
    ))
    && $vars['elements']['#block']->delta !== 'static-block-front'
    && $vars['elements']['#block']->delta !== 'carousel'
  ) {
    // Hide title block.
    $vars['elements']['#block']->subject = NULL;
  }

  if (
    $vars['elements']['#block']->delta == 'static-block-front'
    || $vars['elements']['#block']->delta == 'carousel'
  ) {
    $vars['elements']['#block']->subject = '<span>' . $vars['elements']['#block']->subject . '</span>';
  }

  if ($vars['block']->region == 'footer') {
    $vars['classes_array'][] = 'col-md-4';
  }
}

/**
 * Implementation template_preprocess_views_view().
 */
function sansara_preprocess_views_view(&$vars) {
  // Carousel page.
  if ($vars['view']->name == 'collections' && $vars['view']->current_display == 'page_1') {
    drupal_add_js(drupal_get_path('theme', 'sansara') . '/js/jquery.cloud9carousel.js', 'file');
    $vars['classes_array'][] = 'count' . count($vars['view']->result);
  }
}

/**
 * Get count items in the basket.
 */
function sansara_get_count_items($html = TRUE) {
  global $user;
  $order = commerce_cart_order_load($user->uid);
  $sum = is_object($order) && isset($order->commerce_line_items['und']) ? count($order->commerce_line_items['und']) : 0;
  return $html ? ' <span>(' . $sum . ')</span>' : $sum;
}

/**
 * Sessions for anonimus.
 */
function sansara_lists_session($key, $value = NULL) {
  global $user;
  static $storage;
  // If the user is anonymous, force a session start.
  if ($user->uid == 0) {
    drupal_session_start();
  }
  if ($value) {
    $storage[$key] = $value;
    $_SESSION['lists'][$key] = $value;   // I use 'lists' in case some other module uses 'type' in $_SESSION
  }
  else {
    if (empty($storage[$key]) && isset($_SESSION['lists'][$key])) {
      $storage[$key] = $_SESSION['lists'][$key];
    }
  }
  return $storage[$key];
}

/**
 * Implementation hook_node_view_alter().
 *
 * Added class row for flex-slider.
 */
function sansara_node_view_alter(&$build) {
  if ($build['#view_mode'] == 'full' && $build['#bundle'] == 'product') {
    $build['field_images']['#post_render'][] = 'sansara_field_images_post_render';
  }
}

/**
 * Added class in slider for product page.
 */
function sansara_field_images_post_render($element) {
  return str_replace('optionset-product', 'optionset-product row', $element);
}

/**
 * Added name color to title in product page.
 */
function sansara_field_variation_post_render($element) {
  preg_match('/checked.*title="(.*)"/i', $element, $matches);
  $color = t('Color') . ': ';
  $color .= isset($matches[1]) ? '<span>' . $matches[1] . '</span>' : '';
  $pattern = '/(edit-attributes-field-color.*">)(\w.*)(<\/label>)/i';
  return preg_replace($pattern, "$1$color$3", $element);
}

/**
 * Redirect to views search by title.
 */
function sansara_search_redirect(&$form, &$form_state) {
  if (isset($form_state['values']['search_block_form'])) {
    $options = array('query' => array('title' => $form_state['values']['search_block_form']));
    drupal_goto('/search', $options);
  }
}

/**
 * Redirect to profile view after save new profile.
 */
function sansara_profile_redirect(&$form, &$form_state) {
  $form_state['redirect'] = '/user';
}

/**
 * Implementation hook_form _alter().
 */
function sansara_form_alter(&$form, &$form_state, $form_id) {
  global $user;
  switch ($form['#id']) {

    case 'commerce-customer-ui-customer-profile-form' :
      if (!user_has_role(3)) {
        $form['actions']['save_continue']['#access'] = FALSE;
        $form['user']['name']['#default_value'] = $user->name;
        $form['commerce_customer_address']['#access'] = FALSE;
        $form['profile_status']['#access'] = FALSE;
        $form['user']['#access'] = FALSE;
        foreach (
          array(
            'field_street',
            'field_phone_number',
            'field_apartment_suite_unit',
            'field_town_city',
            'field_zip_postal_code',
            'field_contact_name',
          ) as $i
        ) {
          $title = $form[$i]['und'][0]['value']['#title'];
          $form[$i]['und'][0]['value']['#attributes']['placeholder'] = t($title);
          $form[$i]['und'][0]['value']['#title_display'] = 'invisible';
        }
        $form['#attributes']['class'][] = 'profile-form';
        $form['info']['#markup'] = '<div class="profile-info">';
        $form['info']['#markup'] .= t('Edit your customer details, phone number or delivery address');
        $form['info']['#markup'] .= '</div>';
        $form['go_back']['#markup'] = '<div class="go-back">';
        $form['go_back']['#markup'] .= l(t('Go back shopping'), '');
        $form['go_back']['#markup'] .= '</div>';
        $form['go_back']['#weight'] = 100;
        $form['actions']['#weight'] = 99;
        unset($form['actions']['submit']['#suffix']);
        $form['actions']['submit']['#value'] = t('Save changes');
        drupal_set_title(t('My info'));
        $form['actions']['submit']['#submit'][] = 'sansara_profile_redirect';
      }

      break;

    case 'search-block-form' :
      $form['#post_render'][] = 'sansara_search_block_post_render';
      $form['#submit'][] = 'sansara_search_redirect';
      break;

    case 'user-login' :
      sansara_form_placeholder($form, array('name'));
      sansara_form_placeholder($form, array('pass'));
      $form['#attributes']['class'][] = 'user-login';
      $form['remember_me']['#title'] = t('Keep me signed in');
      $form['name']['#title'] = t('E-mail');
      $form['actions']['submit']['#value'] = t('Enter');
      $options['attributes']['class'][] = 'bold';
      $form['links'] = array(
        '#theme' => 'item_list',
        '#items' => array(
          l(t('Forgotten your password?'), '/user/password'),
          t('Don’t have an account?') . ' ' . l(t('Register'), '/user/register', $options),
        ),
      );
      $form['links']['#attributes']['class'][] = 'login-links';
      $form['actions']['#weight'] = 10;
      $form['links']['#weight'] = 11;
      break;

    case 'user-register-form' :
      sansara_form_placeholder($form, array('account', 'name'));
      sansara_form_placeholder($form, array('account', 'mail'));
      $form['#attributes']['class'][] = 'user-register-form';
      $form['actions']['submit']['#value'] = t('Create account');
      $form['account']['pass']['#pre_render'][] = 'sansara_register_pre_render';
      $text = '<div class="police-text">';
      $text .= '<div>' . t('By clicking on ‘CREATE ACCOUNT’, you accept our') . '</div>';
      $text .= '<div>' . l(t('Confidentiality Policy'), 'node/' . variable_get('sansara_policy', 167)) . '</div>';
      $text .= '</div>';
      $form['policy']['#markup'] = $text;
      $form['actions']['#weight'] = 10;
      $form['policy']['#weight'] = 11;
      $form['simplenews']['#access'] = FALSE;
      break;

    case 'user-pass' :
      sansara_form_placeholder($form, array('name'));
      break;

    case 'views-form-commerce-cart-form-default' :
      $icon = '<span class="icon glyphicon glyphicon-trash" aria-hidden="true"></span>';
      foreach ($form['edit_delete'] as $key => $i) {
        if (!isset($form['edit_delete'][$key]['#value'])) {
          continue;
        }
        $form['edit_delete'][$key]['#value'] = $icon;
      }

      $form['actions']['checkout']['#value'] = t('Buy All');
      $options['attributes']['class'][] = 'remove-url';

      $form['promo'] = array(
        '#prefix' => '<div class="wrap-promo clearfix">' . l(t('Remove All'), '/cart/empty', $options),
        '#type' => 'textfield',
        '#attributes' => array('placeholder' => 'PROMOCODE'),
        '#weight' => 97,
        '#suffix' => '</div>'
      );
      $order = commerce_cart_order_load($user->uid);
      $amount = ($order->commerce_order_total['und'][0]['amount'] / 100);
      $code = commerce_multicurrency_get_user_currency_code();
      $currency = commerce_currency_load($code);
      $sum = is_object($order) ? count($order->commerce_line_items['und']) : 0;
      switch ($code) {
        case 'USD' :
          $total = sansara_helper_commerce_currency_format_USD($amount, $currency);
          break;
        case 'EUR' :
          $total = sansara_helper_commerce_currency_format_EUR($amount, $currency);
          break;
      }
      $form['sub_total'] = array(
        '#prefix' => '<div class="sub-total">',
        '#markup' => t('Subtotal (@total items):', array('@total' => $sum)) . ' <span>' . $total . '</span>',
        '#suffix' => '</div>',
        '#weight' => 98,
      );
      $form['#action'] = '/cart';
      break;

    case 'commerce-customer-ui-customer-profile-form' :
      $form['commerce_customer_address']['#access'] = FALSE;
      $form['profile_status']['#access'] = FALSE;
      $form['user']['#access'] = FALSE;
      foreach (
        array(
          'field_street',
          'field_phone_number',
          'field_apartment_suite_unit',
          'field_town_city',
          'field_zip_postal_code',
          'field_contact_name',
        ) as $i
      ) {
        $title = $form[$i]['und'][0]['value']['#title'];
        $form[$i]['und'][0]['value']['#attributes']['placeholder'] = t($title);
        $form[$i]['und'][0]['value']['#title_display'] = 'invisible';
      }
      $form['#attributes']['class'][] = 'profile-form';
      $form['info']['#markup'] = '<div class="profile-info">';
      $form['info']['#markup'] .= t('Edit your customer details, phone number or delivery address');
      $form['info']['#markup'] .= '</div>';
      $form['go_back']['#markup'] = '<div class="go-back">';
      $form['go_back']['#markup'] .= l(t('Go back shopping'), '');
      $form['go_back']['#markup'] .= '</div>';
      $form['go_back']['#weight'] = 100;
      $form['actions']['#weight'] = 99;
      unset($form['actions']['submit']['#suffix']);
      $form['actions']['submit']['#value'] = t('Save changes');
      drupal_set_title(t('My info'));
      break;

    case 'user-profile-form' :
      $form['subscriptions']['#description'] = t('Decide which newsletters you would like to RECEive.');
      $form['subscriptions']['#description'] .= '<div class="subs-title">' . t('Newsletters') . '</div>';
      $form['go_back']['#markup'] = '<div class="go-back">';
      $form['go_back']['#markup'] .= l(t('Go back shopping'), '');
      $form['go_back']['#markup'] .= '</div>';
      $form['go_back']['#weight'] = 100;
      $form['actions']['#weight'] = 99;
      break;
  }

  // My orders.
  if (strpos($form['#id'], 'views-form-commerce-cart-form-block-2') !== FALSE) {
    $form['actions']['#access'] = FALSE;
    if (isset($form['edit_quantity']) && count($form['edit_quantity']) > 0) {
      foreach ($form['edit_quantity'] as $key => $i) {
        if (is_array($i) && $i['#type'] == 'textfield') {
          // Disabled quantity count field.
          $form['edit_quantity'][$key]['#attributes']['disabled'][] = 'disabled';
        }
      }
    }
  }

  if (strpos($form['#id'], 'commerce-checkout-form-review') !== FALSE) {
    $form['commerce_payment']['payment_details']['credit_card']['exp_month']['#suffix'] = '';
    $form['commerce_payment']['payment_details']['credit_card']['exp_month']['#title'] = '';
    unset($form['help']);
    $form['buttons']['back']['#access'] = FALSE;
    $form['buttons']['continue']['#value'] = t('COMFIRM PAYMENT');
    $card = '<img class="img-responsive" src="/sites/all/themes/sansara/images/card.png">';
    $paypal = '<img class="img-responsive" src="/sites/all/themes/sansara/images/paypal.png">';
    if (module_exists('commerce_paypal_wpp')) {
      $form['commerce_payment']['payment_method']['#options']['paypal_wpp|commerce_payment_paypal_wpp'] = $card;
    }
    else {
      $form['commerce_payment']['payment_details']['#access'] = FALSE;
    }
    $form['commerce_payment']['payment_method']['#options']['paypal_wps|commerce_payment_paypal_wps'] = $paypal;
    $metod = isset($_POST['commerce_payment']['payment_method']) ? (string) $_POST['commerce_payment']['payment_method'] : '';
    $label = '<div class="wrap-expire">';
    $label .= $metod == 'paypal_wps|commerce_payment_paypal_wps' ? '' : '<label>' . t('Expire Date') . '</label>';
    $label .= '<div class="inner-expire">';
    $form['commerce_payment']['payment_details']['credit_card']['exp_month']['#prefix'] = $label;
    $form['commerce_payment']['payment_details']['credit_card']['exp_year']['#suffix'] = '</div></div>';

    $order = commerce_cart_order_load($user->uid);
    $wrapper = entity_metadata_wrapper('commerce_order', $order);
    $order_total = $wrapper->commerce_order_total->value()['amount'];

    $html = '<div class="total row">';
    $html .= '<div class="col-md-4">' . t('Total') . ':' . '</div>';
    $html .= '<div class="col-md-8">' . substr($order_total, 0, 3) . ' $' . '</div>';
    $html .= '</div>';
    $form['total'] = array(
      '#markup' => $html,
      '#weight' => 1
    );
    $form['buttons']['#weight'] = 2;
    $form['link'] = array(
      '#prefix' => '<div class="payment-link">',
      '#suffix' => '</div>',
      '#markup' => l(t('Return to shopping cart'), '/cart'),
      '#weight' => 3
    );
    $info = '<span class="info"></span>';
    $form['commerce_payment']['payment_details']['credit_card']['code']['#title'] = t('CVC');
    $form['commerce_payment']['payment_details']['credit_card']['code']['#field_suffix'] = $info;
  }

  if (strpos($form['#id'], 'commerce-checkout-form-checkout') !== FALSE) {
    unset($form['buttons']['back']);
    unset($form['buttons']['cancel']);
    $ph = 'customer_profile_shipping';
    $pb = 'customer_profile_billing';
    // For default billing, used access for some fields.
    $access = (isset($form[$pb]['edit_button'])) ? FALSE : TRUE;
    $form[$pb]['#attributes']['class'][] = $access ? 'edit' : 'default';
    $form[$ph]['#attributes']['class'][] = $access ? 'edit' : 'default';
    $form['buttons']['#attributes']['class'][] = $access ? 'e-btn' : 'd-btn';
    if ($access) {
      $btn = t('SAVE & SHIP TO THIS ADDRESS');
    }
    else {
      $btn = t('CONFIRM & SHIP TO THIS ADDRESS');
    }

    $form[$ph]['commerce_customer_profile_copy']['#default_value'] = 1;
    $form[$ph]['commerce_customer_profile_copy']['#access'] = $access;
    $form[$ph]['#access'] = $access;

    foreach (
      array(
        'field_contact_name',
        'field_town_city',
        'field_street',
        'field_apartment_suite_unit',
        'field_zip_postal_code',
        'field_phone_number'
      ) as $key => $i) {
      // Billing.
      if (isset($form[$pb][$i]['und'][0]['value'])) {
        $title = $form[$pb][$i]['und'][0]['value']['#title'];
        $form[$pb][$i]['und'][0]['value']['#attributes']['placeholder'] = $title;
        $form[$pb][$i]['und'][0]['value']['#title_display'] = 'invisible';
      }
      // Shipping.
      if (isset($form[$ph][$i]['und'][0]['value'])) {
        $title = $form[$ph][$i]['und'][0]['value']['#title'];
        $form[$ph][$i]['und'][0]['value']['#attributes']['placeholder'] = $title;
        $form[$ph][$i]['und'][0]['value']['#title_display'] = 'invisible';
      }
    }
    $form['invoice'] = array(
      '#prefix' => '<div class="invoice">',
      '#markup' => t('Invoice address'),
      '#suffix' => '</div>',
      '#access' => $access
    );

    // Added new adress invoice.
    $form[$ph]['commerce_customer_profile_copy']['#title'] = '';
    $form['profile_copy'] = array(
      '#type' => 'radios',
      '#options' => array(
        1 => t('Add new'),
        0 => t('Use the same'),
      ),
      '#default_value' => 0,
      '#access' => $access
    );
    $form['profile_copy']['#attributes']['class'][] = 'trigger-copy';
    $form['customer_profile_shipping']['#weight'] = 1;
    $form['check_shiping'] = array(
      '#type' => 'checkbox',
      '#title' => t('Set this shiping adress as default'),
      '#access' => $access,
      '#weight' => 2
    );
    $form['buttons']['continue']['#value'] = $btn;
    $form['buttons']['#weight'] = 100;
  }

  // Categories page.
  if (strpos($form['#id'], 'views-exposed-form-catalog-page') !== FALSE
    && isset($form['field_color_tid']['#options'])
    && count($form['field_color_tid']['#options']) > 0
  ) {
    foreach ($form['field_color_tid']['#options'] as $key => $i) {
      if ($key > 0) {
        $term_wrapper = entity_metadata_wrapper('taxonomy_term', $key);
        $icon = $term_wrapper->field_icon->value();
        $form['field_color_tid']['#options'][$key] = theme('image_style', array(
          'style_name' => 'icon',
          'path' => $icon['uri'],
          'attributes' => array('class' => array('icon')),
        ));
      }
    }
  }

  // Icons for color on product page.
  if (strpos($form['#id'], 'commerce-cart-add-to-cart-form') !== FALSE &&
    isset($form['attributes']['field_color']['#options']) &&
    count($form['attributes']['field_color']['#options']) > 0
  ) {
    $options['attributes']['class'][] = 'sizes-link';
    $form['attributes']['#suffix'] = l('Sizes guide', 'node/' . variable_get('sansara_sizes_guide', '3'), $options) . '</div>';
    $form['submit']['#value'] = t('Add to shopping cart');
    $form['attributes']['field_color']['#post_render'][] = 'sansara_field_variation_post_render';
    foreach ($form['attributes']['field_color']['#options'] as $key => $i) {
      $term_wrapper = entity_metadata_wrapper('taxonomy_term', $key);
      $icon = $term_wrapper->field_icon->value();
      $form['attributes']['field_color']['#options'][$key] = theme('image_style', array(
        'style_name' => 'icon',
        'path' => $icon['uri'],
        'attributes' => array('class' => array('icon')),
        'title' => $form['attributes']['field_color']['#options'][$key]
      ));
    }
  }

  // Simplenews.
  if (strpos($form['#id'], 'simplenews-block-form') !== FALSE) {
    $form['mail']['#title'] = t('Enter your e-mail');
    sansara_form_placeholder($form, array('mail'));
    $form['submit']['#value'] = t('Submit');
  }
}

/**
 * Added placeholders for form fields.
 */
function sansara_form_placeholder(&$form, $e) {
  switch (count($e)) {
    case '1' :
      $form[$e[0]]['#title_display'] = 'invisible';
      $form[$e[0]]['#attributes']['placeholder'] = $form[$e[0]]['#title'];
      break;
    case '2' :
      $form[$e[0]][$e[1]]['#title_display'] = 'invisible';
      $form[$e[0]][$e[1]]['#attributes']['placeholder'] = $form[$e[0]][$e[1]]['#title'];
      break;
  }
}

/**
 * Added placeholders for fields confirm pass.
 */
function sansara_register_pre_render($element) {
  sansara_form_placeholder($element, array('pass1'));
  sansara_form_placeholder($element, array('pass2'));
  return $element;
}

/**
 * Added clearfix class to fields on payment form.
 */
function sansara_payment_post_render($element) {
  return str_replace('form-item', 'form-item clearfix', $element);
}

/**
 * Added trigger button for search.
 */
function sansara_search_block_post_render($element) {
  return str_replace('</form>', '</form><span title="Search" class="search-trigger"><a class="search-link" href="/search"></a></span>', $element);
}

/**
 * Added wrapper for the primary menu links.
 */
function sansara_menu_tree__primary(&$vars) {
  $vars['tree'] = preg_replace('/(<a.*href=".*active.*">)(.*)(<\/a>)/i', '$1<span>$2</span>$3', $vars['tree']);
  return '<ul class="menu nav navbar-nav">' . $vars['tree'] . '</ul>';
}

/**
 * Get profile ID.
 */
function sansara_get_pid($uid = NULL) {
  global $user;
  $profile = commerce_single_address_active_profile_load($uid ? $uid : $user->uid, 'billing');
  if ($uid) {
    return $profile ? $profile : FALSE;
  }
  return $profile ? $profile->profile_id : FALSE;
}